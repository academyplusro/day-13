package com.example.demo;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Welcome extends ResourceSupport {

    private final String search;

    @JsonCreator
    public Welcome(@JsonProperty("search") String search) {
        this.search = search;
    }

    public String getSearch() {
        return search;
    }
}