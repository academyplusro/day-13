package com.example.demo;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class WelcomeController {

    private static final String TEMPLATE = "Welcome, %s!";

    @RequestMapping("/welcome")
    public HttpEntity<Welcome> welcome(
            @RequestParam(value = "name", required = false, defaultValue = "Student") String name) {

        Welcome welcome = new Welcome(String.format(TEMPLATE, name));
        welcome.add(linkTo(methodOn(WelcomeController.class).welcome(name)).withSelfRel());

        return new ResponseEntity<Welcome>(welcome, HttpStatus.OK);
    }
}