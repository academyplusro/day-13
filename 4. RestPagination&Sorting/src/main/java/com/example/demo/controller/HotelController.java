package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.data.HotelRepository;
import com.example.demo.model.Hotel;


@RestController
public class HotelController {

	@Autowired
	private HotelRepository hotelData;

	@RequestMapping(value = "/listPageable", method = RequestMethod.GET)
	Page<Hotel> hotelsPageable(Pageable pageable) {
		return hotelData.findAll(pageable);

	}

}