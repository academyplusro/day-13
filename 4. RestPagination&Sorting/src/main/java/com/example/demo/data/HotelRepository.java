package com.example.demo.data;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Hotel;


public interface HotelRepository extends JpaRepository<Hotel
			, Long> {

}