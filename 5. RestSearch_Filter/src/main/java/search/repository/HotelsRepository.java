package search.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import search.model.Hotels;

import java.util.List;

public interface HotelsRepository extends ElasticsearchRepository<Hotels, Long> {
    List<Hotels> findByName(String text);

    List<Hotels> findById(Long id);
}
