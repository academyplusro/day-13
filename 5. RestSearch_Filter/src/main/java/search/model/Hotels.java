package search.model;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "hotels", type = "hotels", shards = 1)
public class Hotels {

	private String name;
	private Long id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Hotels(String name, Long id) {

		this.name = name;
		this.id = id;
	}

	public Hotels() {

	}

}
