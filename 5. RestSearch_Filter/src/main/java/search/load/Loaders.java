package search.load;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import search.model.Hotels;
import search.repository.HotelsRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class Loaders {

    @Autowired
    ElasticsearchOperations operations;

    @Autowired
    HotelsRepository hotelsRepository;

    @PostConstruct
    @Transactional
    public void loadAll(){

        operations.putMapping(Hotels.class);
        System.out.println("Loading Data");
        hotelsRepository.save(getData());
        System.out.printf("Loading Completed");

    }

    private List<Hotels> getData() {
        List<Hotels> hotels = new ArrayList<>();
        hotels.add(new Hotels("Havana",15L));
        hotels.add(new Hotels("Nevis&Spa",16L));
        hotels.add(new Hotels("Continental",17L));
        return hotels;
    }
}
