package search.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import search.model.Hotels;
import search.repository.HotelsRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest/search")
public class SearchResource {

    @Autowired
    HotelsRepository hotelsRepository;

    @GetMapping(value = "/name/{text}")
    public List<Hotels> searchName(@PathVariable final String text) {
        return hotelsRepository.findByName(text);
    }


    @GetMapping(value = "/id/{id}")
    public List<Hotels> searchId(@PathVariable final String id) {
        return hotelsRepository.findById(Long.valueOf(id));
    }


    @GetMapping(value = "/all")
    public List<Hotels> searchAll() {
        List<Hotels> hotelsList = new ArrayList<>();
        Iterable<Hotels> hotels = hotelsRepository.findAll();
        hotels.forEach(hotelsList::add);
        return hotelsList;
    }


}
