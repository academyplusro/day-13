package com.example.demo;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HotelController {
    
	@RequestMapping(value = "/hotels" , method = RequestMethod.GET)
	public ArrayList<Hotel>getHotelList(){
		
		Hotel hotel1 = new Hotel();
		hotel1.setName("Ritz Hotel");
		hotel1.setRating("High");
		hotel1.setStars( 5 );
		Hotel hotel2 = new Hotel();
		hotel2.setName("DayDream Hotel");
		hotel2.setRating("Medium");
		hotel2.setStars(3);
		Hotel hotel3 = new Hotel();
		hotel3.setName("Green Hotel");
		hotel3.setRating("Low");
		hotel3.setStars(4);
		ArrayList<Hotel> hotelList = new ArrayList<Hotel>();
		hotelList.add(hotel1);
		hotelList.add(hotel2);
		hotelList.add(hotel3);
		
		return hotelList;
	}
	
	
	@RequestMapping(value = "/hotels",method = RequestMethod.PUT)
	public String updatehotel1(@RequestBody String stars) {
		return "The new stars for Ritz Hotel are " + stars;
	} 
		
		
}


