package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StatusController {

	@RequestMapping(value= "/400")
	public String badRequest() {
		return "400";
	}
	
	@RequestMapping(value= "/404")
	public String notFound() {
		return "404";
	}
	
	@RequestMapping(value= "/401")
	public String unathorized() {
		return "401";
	}
	
	@RequestMapping(value= "/403")
	public String forbidden() {
		return "403";
	}
	
	@RequestMapping(value= "/200")
	public String succeeded() {
		return "200";
	}
	
	@RequestMapping(value= "/500")
	public String error() {
		return "500";
	}
	
}
