package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpStatusCodesApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpStatusCodesApplication.class, args);
	}
}
