package main.services.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import main.model.Hotel;

@RestController
public class WebController {
	
	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public String post(@RequestBody Hotel hot) {
		System.out.println("/POST request, hot: " + hot.toString());
		return "/Post Successful!";
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public Hotel get(@RequestParam("id") long id, @RequestParam("name") String name) {
		String info = String.format("/GET info: id=%d, name=%s", id, name);
		System.out.println(info);
		return new Hotel(id, name);
	}

	@RequestMapping(value= "/put/{id}", method = RequestMethod.PUT)
	public void put(@PathVariable(value = "id") long id, @RequestBody Hotel hot) {
		String info = String.format("id = %d, hotinfo = %s", id, hot.toString());
		System.out.println("/PUT info " + info);
	}
	
	@RequestMapping(value= "/delete/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable(value = "id") long id) {
		String info = String.format("/Delete info: id = %d", id);
		System.out.println(info);
	}

}
