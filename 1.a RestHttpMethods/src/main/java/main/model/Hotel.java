package main.model;

public class Hotel {
	private long id;
	private String name;
	
	public Hotel(){
		
	}
	
	public Hotel(long id, String name){
		this.id = id;
		this.name = name;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	@Override
	public String toString() {
		String info = String.format("Hotel Info: id = %d, name = %s", id, name);
		return info;
	}

}
