package demo;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import demo.storage.Storage;

@SpringBootApplication
public class SpringBootUploadFileApplication implements CommandLineRunner{

	@Resource
	Storage storage;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootUploadFileApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	
		
	}

	
}
