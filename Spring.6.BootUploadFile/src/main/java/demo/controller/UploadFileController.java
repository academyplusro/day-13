package demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import demo.storage.Storage;

@Controller
public class UploadFileController {

	@Autowired
	Storage storage;

	List<String> files = new ArrayList<String>();

	@GetMapping("/")
	public String listOfUploadedFiles(Model model) {
		return "uploadForm";
	}

	@PostMapping("/")
	public String handleFileUpload(@RequestParam("file") MultipartFile file, Model model) {
		try {
			storage.store(file);
			model.addAttribute("message", "You successfully uploaded " + file.getOriginalFilename() + "!");
			files.add(file.getOriginalFilename());
		} catch (Exception e) {
			model.addAttribute("message", "Fail to upload " + file.getOriginalFilename() + "!");
		}
		return "uploadForm";
	}


}